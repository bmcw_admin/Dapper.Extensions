﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dapper.Extensions.Common
{

    /// <summary>
    /// 排序信息
    /// </summary>
    public class Sort
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="dir"></param>
        private Sort(string propertyName,EnumSortDirection dir=EnumSortDirection.Ascending)
        {
            PropertyName = propertyName;
            Direction = dir;
        }
        /// <summary>
        /// 创建一个排序对象，唯一方法
        /// </summary>
        /// <param name="sortFieldExpression">排序字段表达式</param>
        /// <param name="direction">排序方向</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Sort CreateSort<T>(Expression<Func<T, object>> sortFieldExpression, EnumSortDirection direction = EnumSortDirection.Ascending) where T:class
        {
            return new Sort(SqlGenerator.GetMemberNameFromExpression(sortFieldExpression), direction);

        }
     

       
        /// <summary>
        /// 属性名称
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// 排序方向
        /// </summary>
        public EnumSortDirection Direction { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SortWrapper<T>
    {
       /// <summary>
       /// 
       /// </summary>
       public Expression<Func<T, object>> sortFieldExpression { get; set; }
       /// <summary>
       /// 
       /// </summary>
       public EnumSortDirection Direction { get; set; }
    }
    /// <summary>
    /// 排序方向
    /// </summary>
    public enum EnumSortDirection
    {
        /// <summary>
        /// 升序
        /// </summary>
        Ascending = 0,

        /// <summary>
        /// 降序
        /// </summary>
        Descending
    }


}
