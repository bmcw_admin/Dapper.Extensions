﻿// =================================================================== 
// 项目说明
//====================================================================
// Sushee @Copy Right 2019
// 文件： sms_sendEntity.cs
// 项目名称：斑马车务
// 创建时间：2019/9/20
// 负责人：Sushee
// ===================================================================

using System;
using Dapper.Extensions;

namespace YouWei.Infrastructure.Entity
{
	/// <summary>
	///sms_send数据实体  
	/// </summary>
    [PrimaryKey("Id")]
	public class sms_send
	{
		///<summary>
		///sms_send
		///</summary>
		public sms_send()
		{
		}	
    
		
		///<summary>
		///
		///</summary>
        [Column("Id","")]
        public int Id { get; set; }

		///<summary>
		///大于0表示指定端口号发送
		///</summary>
        [Column("PortNum","大于0表示指定端口号发送")]
        public int PortNum { get; set; }

		///<summary>
		///接收号码
		///</summary>
        [Column("smsNumber","接收号码")]
        public string smsNumber { get; set; }

		///<summary>
		///彩信标题，如果发送彩信不能为空
		///</summary>
        [Column("smsSubject","彩信标题，如果发送彩信不能为空")]
        public string smsSubject { get; set; }

		///<summary>
		///发送内容
		///</summary>
        [Column("smsContent","发送内容")]
        public string smsContent { get; set; }

		///<summary>
		///0:短信 1:彩信
		///</summary>
        [Column("smsType","0:短信 1:彩信")]
        public uint smsType { get; set; }

		///<summary>
		///手机号
		///</summary>
        [Column("PhoNum","手机号")]
        public string PhoNum { get; set; }

		///<summary>
		///0:未发送 1:已在发送队列 2:发送成功 3:发送失败
		///</summary>
        [Column("smsState","0:未发送 1:已在发送队列 2:发送成功 3:发送失败")]
        public uint smsState { get; set; }

	}
}