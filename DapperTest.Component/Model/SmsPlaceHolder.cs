﻿// =================================================================== 
// 项目说明
//====================================================================
// sushee @Copy Right 2019
// 文件： SmsPlaceHolderEntity.cs
// 项目名称：斑马车务
// 创建时间：2019/9/25
// 负责人：sushee
// ===================================================================

using System;
using Dapper.Extensions;

namespace YouWei.Infrastructure.Entity
{
    /// <summary>
    ///数据实体  
    /// </summary>
    [PrimaryKey("PlaceHolderID")]
    public class SmsPlaceHolder
    {
        ///<summary>
        ///
        ///</summary>
        public SmsPlaceHolder()
        {
        }


        ///<summary>
        ///
        ///</summary>
        [Column("PlaceHolderID", "")]
        public int PlaceHolderID { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Column("TemplateID", "")]
        public int TemplateID { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Column("OrderNumber", "")]
        public int OrderNumber { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Column("PlaceHolderText", "")]
        public string PlaceHolderText { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Column("CarBrand", "")]
        public string CarBrand { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Column("CarSerial", "")]
        public string CarSerial { get; set; }

    }
}