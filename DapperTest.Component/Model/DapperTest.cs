﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Extensions;

namespace DapperDemo.Component.Model
{
    [PrimaryKey("UserId")]
    public class DapperTest
    {
        public int UserId { get; set; }
        public string UserName { get; set; }

        public float FloatValue { get; set; }

        public decimal DecimalValue { get; set; }

        public double NumberValue { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime TestDate1 { get; set; }
        public DateTime TestDate2 { get; set; }
    }

}
