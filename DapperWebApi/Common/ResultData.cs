﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace DapperDemo.Api
{
    /// <summary>
    /// 统一返回结果,用于不需要返回具体结果实体的方法调用中，即只包含status和message两个属性,请不要擅自修改改类的任何地方 
    /// </summary>
    public class ResultData
    {
        private ResultData()
        {
            
        }
        /// <summary>
        /// 返回状态
        /// 0：成功,否则失败
        /// </summary>
        public int status { set; get; }
      
        /// <summary>
        /// 错误信息
        /// </summary>
        public string message { set; get; }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ResultData Success()
        {
            ResultData result = new ResultData
            {
                status = 0,
                message = "",
            };
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ResultData Success(string msg)
        {
            ResultData result = new ResultData
            {
                status = 0,
                message = msg,
            };
            return result;
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static ResultData Error(int errorCode,string errMsg)
        {
            ResultData result = new ResultData
            {
                status = (int)errorCode,
                message = errMsg
            };
            return result;
        }
    }

    /// <summary>
    /// 统一返回结果泛型版本,通常用于包含返回结果的调用方法中，,请不要擅自修改改类的任何地方 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResultData<T> where T:new()
    {
        private ResultData()
        {
            
        }
      
        /// <summary>
        /// 错误信息
        /// </summary>
        public string message { set; get; }

        /// <summary>
        /// 返回状态
        /// 0：成功,否则失败
        /// </summary>
        public int status { set; get; }


        /// <summary>
        /// 返回信息
        /// </summary>
        public T data { set; get; }

        /// <summary>
        /// 成功方法
        /// </summary>
        /// <param name="_data">返回对象</param>
        /// <returns></returns>
        public  ResultData<T> Success(T _data)
        {
            ResultData<T> result = new ResultData<T>
            {
                status =0,
                message = string.Empty,
                data = _data
            };
            return result;
        }

        /// <summary>
        /// 成功方法
        /// </summary>
        /// <param name="_data">返回数据</param>
        /// <param name="msg">返回成功文本</param>
        /// <returns></returns>
        public  ResultData<T> Success(T _data,string msg)
        {
            ResultData<T> result = new ResultData<T>
            {
                status = 0,
                message = msg,
                data = _data
            };
            return result;
        }

        /// <summary>
        /// 成功方法
        /// </summary>
        /// <param name="msg">返回信息</param>
        /// <returns></returns>
        public  ResultData<T> Success(string msg)
        {
            ResultData<T> result = new ResultData<T>
            {
                status = 0,
                message = msg,
            };
            return result;
        }

        public static ResultData<T> Error()
        {
            ResultData<T> result = new ResultData<T>
            {
                status = -1,
                message = "未知错误"
            };
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public  ResultData<T> Error(int errorCode, string errMsg)
        {
            ResultData<T> result = new ResultData<T>
            {
                status = (int)errorCode,
                message = errMsg
            };
            return result;
        }

    }


}